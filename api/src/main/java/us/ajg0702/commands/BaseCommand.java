package us.ajg0702.commands;

import com.google.common.collect.ImmutableList;

import java.util.*;
import java.util.function.Function;

@SuppressWarnings("unused")
public abstract class BaseCommand {

    private final String name;

    private final List<SubCommand> subCommands = new ArrayList<>();

    private final List<String> aliases;
    private final String permission;
    private final String description;

    Function<CommandSender, Boolean> showInTabComplete = cs -> true;

    public BaseCommand(String name, List<String> aliases, String permission, String description) {
        this.name = name;
        this.aliases = aliases;
        this.permission = permission;
        this.description = description;
    }

    public void setShowInTabComplete(boolean showInTabComplete) {
        this.showInTabComplete = cs -> showInTabComplete;
    }
    public void setShowInTabComplete(Function<CommandSender, Boolean> showInTabComplete) {
        this.showInTabComplete = showInTabComplete;
    }

    public String getName() {
        return name;
    }

    public ImmutableList<String> getAliases() {
        return ImmutableList.copyOf(aliases);
    }

    public ImmutableList<SubCommand> getSubCommands() {
        return ImmutableList.copyOf(subCommands);
    }

    public String getPermission() {
        return permission;
    }

    public String getDescription() {
        return description;
    }

    public boolean showInTabComplete(CommandSender sender) {
        return showInTabComplete.apply(sender);
    }

    public void addSubCommand(SubCommand subCommand) {
        subCommands.add(subCommand);
    }

    public boolean checkPermission(CommandSender sender) {
        return !(getPermission() != null && !sender.hasPermission(getPermission()));
    }


    public List<String> filterCompletion(List<String> in, String current) {
        List<String> out = new ArrayList<>(in);
        out.removeIf(t -> !t.toLowerCase(Locale.ROOT).contains(current.toLowerCase(Locale.ROOT)));
        return out;
    }

    public List<String> subCommandAutoComplete(CommandSender sender, String[] args) {
        if(args.length > 1) {
            for(SubCommand subCommand : subCommands) {
                if(args[0].equalsIgnoreCase(subCommand.getName()) || subCommand.getAliases().contains(args[0].toLowerCase(Locale.ROOT))) {
                    if(!sender.hasPermission(subCommand.getPermission())) continue;
                    List<String> subCommandCompleted = subCommand.autoComplete(sender, Arrays.copyOfRange(args, 1, args.length));
                    if(subCommandCompleted != null) {
                        subCommandCompleted.removeAll(Collections.singletonList(null));
                    }
                    return subCommandCompleted;
                }
            }
            return new ArrayList<>();
        }
        List<String> commands = new ArrayList<>();
        for(SubCommand subCommand : subCommands) {
            if(!subCommand.showInTabComplete(sender)) continue;
            if(!sender.hasPermission(subCommand.getPermission())) continue;
            commands.add(subCommand.getName());
            if(subCommand.getAliases() != null) {
                commands.addAll(subCommand.getAliases());
            }
        }
        commands.removeAll(Collections.singleton(null));
        return commands;
    }

    public boolean subCommandExecute(CommandSender sender, String[] args, String label) {
        if(args.length < 1) return false;
        for(SubCommand subCommand : subCommands) {
            if(args[0].equalsIgnoreCase(subCommand.getName()) || subCommand.getAliases().contains(args[0].toLowerCase(Locale.ROOT))) {
                subCommand.execute(sender, Arrays.copyOfRange(args, 1, args.length), label);
                return true;
            }
        }
        return false;
    }

    public abstract List<String> autoComplete(CommandSender sender, String[] args);

    public abstract void execute(CommandSender sender, String[] args, String label);

}
