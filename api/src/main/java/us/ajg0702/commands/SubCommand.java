package us.ajg0702.commands;

import java.util.List;

public abstract class SubCommand extends BaseCommand {
    public SubCommand(String name, List<String> aliases, String permission, String description) {
        super(name, aliases, permission, description);
    }
}
