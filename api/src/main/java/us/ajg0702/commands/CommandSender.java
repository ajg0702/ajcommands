package us.ajg0702.commands;

import net.kyori.adventure.audience.Audience;
import us.ajg0702.commands.utils.Handle;

@SuppressWarnings({"BooleanMethodIsAlwaysInverted", "Unused"})
public abstract class CommandSender implements Handle, Audience {
    public abstract boolean hasPermission(String permission);
    public abstract boolean isPlayer();

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof CommandSender)) return false;
        return getHandle().equals(((CommandSender) o).getHandle());
    }
}
