package us.ajg0702.commands.platforms.bukkit;

import org.bukkit.command.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import us.ajg0702.commands.BaseCommand;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@SuppressWarnings("unused")
public class BukkitCommand implements CommandExecutor, TabCompleter {

    private final BaseCommand command;
    public BukkitCommand(BaseCommand command) {
        this.command = command;
    }


    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command bukkitCommand, @NotNull String label, @NotNull String[] args) {
        command.execute(new BukkitSender(commandSender), args, label);
        return true;
    }

    @Override
    public List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command bukkitCommand, @NotNull String s, String[] args) {
        return command.autoComplete(new BukkitSender(commandSender), args);
    }

    public void register(JavaPlugin plugin) {
        register(plugin, command.getName());
    }
    public void register(JavaPlugin plugin, String commandName) {
        PluginCommand command = plugin.getCommand(commandName);
        if(command == null) {
            throw new IllegalStateException("Command not found! Is it in plugin.yml?");
        }
        command.setExecutor(this);
        command.setTabCompleter(this);

        command.setAliases(command.getAliases());
        command.setDescription(command.getDescription());
    }
}
