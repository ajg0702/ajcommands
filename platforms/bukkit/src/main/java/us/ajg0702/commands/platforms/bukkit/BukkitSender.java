package us.ajg0702.commands.platforms.bukkit;

import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.audience.MessageType;
import net.kyori.adventure.bossbar.BossBar;
import net.kyori.adventure.identity.Identified;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.inventory.Book;
import net.kyori.adventure.platform.bukkit.BukkitAudiences;
import net.kyori.adventure.sound.Sound;
import net.kyori.adventure.sound.SoundStop;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.ComponentLike;
import net.kyori.adventure.title.Title;
import net.kyori.adventure.title.TitlePart;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import us.ajg0702.commands.CommandSender;

@SuppressWarnings("unused")
public class BukkitSender extends CommandSender implements Audience {
    private static BukkitAudiences adventure;

    public static void setAdventure(BukkitAudiences bukkitAudiences) {
        adventure = bukkitAudiences;
    }
    public static void setAdventure(Plugin plugin) {
        adventure = BukkitAudiences.create(plugin);
    }
    
    private final Audience audience;

    private final org.bukkit.command.CommandSender handle;
    public BukkitSender(org.bukkit.command.CommandSender handle) {
        this.handle = handle;
        this.audience = adventure.sender(handle);
    }

    @Override
    public boolean hasPermission(String permission) {
        if(permission == null) return true;
        return handle.hasPermission(permission);
    }

    @Override
    public boolean isPlayer() {
        return handle instanceof Player;
    }

    @Override
    public org.bukkit.command.CommandSender getHandle() {
        return handle;
    }


    @Override
    public void sendMessage(@NotNull ComponentLike message) {
        audience.sendMessage(message);
    }

    @Override
    public void sendMessage(@NotNull Identified source, @NotNull ComponentLike message) {
        audience.sendMessage(source, message);
    }

    @Override
    public void sendMessage(@NotNull Identity source, @NotNull ComponentLike message) {
        audience.sendMessage(source, message);
    }

    @Override
    public void sendMessage(@NotNull Component message) {
        audience.sendMessage(message);
    }

    @Override
    public void sendMessage(@NotNull Identified source, @NotNull Component message) {
        audience.sendMessage(source, message);
    }

    @Override
    public void sendMessage(@NotNull Identity source, @NotNull Component message) {
        audience.sendMessage(source, message);
    }

    @Override
    public void sendMessage(@NotNull ComponentLike message, @NotNull MessageType type) {
        audience.sendMessage(message, type);
    }

    @Override
    public void sendMessage(@NotNull Identified source, @NotNull ComponentLike message, @NotNull MessageType type) {
        audience.sendMessage(source, message, type);
    }

    @Override
    public void sendMessage(@NotNull Identity source, @NotNull ComponentLike message, @NotNull MessageType type) {
        audience.sendMessage(source, message, type);
    }

    @Override
    public void sendMessage(@NotNull Component message, @NotNull MessageType type) {
        audience.sendMessage(message, type);
    }

    @Override
    public void sendMessage(@NotNull Identified source, @NotNull Component message, @NotNull MessageType type) {
        audience.sendMessage(source, message, type);
    }

    @Override
    public void sendMessage(@NotNull Identity source, @NotNull Component message, @NotNull MessageType type) {
        audience.sendMessage(source, message, type);
    }

    @Override
    public void sendActionBar(@NotNull ComponentLike message) {
        audience.sendActionBar(message);
    }

    @Override
    public void sendActionBar(@NotNull Component message) {
        audience.sendActionBar(message);
    }

    @Override
    public void sendPlayerListHeader(@NotNull ComponentLike header) {
        audience.sendPlayerListHeader(header);
    }

    @Override
    public void sendPlayerListHeader(@NotNull Component header) {
        audience.sendPlayerListHeader(header);
    }

    @Override
    public void sendPlayerListFooter(@NotNull ComponentLike footer) {
        audience.sendPlayerListFooter(footer);
    }

    @Override
    public void sendPlayerListFooter(@NotNull Component footer) {
        audience.sendPlayerListFooter(footer);
    }

    @Override
    public void sendPlayerListHeaderAndFooter(@NotNull ComponentLike header, @NotNull ComponentLike footer) {
        audience.sendPlayerListHeaderAndFooter(header, footer);
    }

    @Override
    public void sendPlayerListHeaderAndFooter(@NotNull Component header, @NotNull Component footer) {
        audience.sendPlayerListHeaderAndFooter(header, footer);
    }

    @Override
    public void showTitle(@NotNull Title title) {
        audience.showTitle(title);
    }

    @Override
    public <T> void sendTitlePart(@NotNull TitlePart<T> part, @NotNull T value) {
        audience.sendTitlePart(part, value);
    }

    @Override
    public void clearTitle() {
        audience.clearTitle();
    }

    @Override
    public void resetTitle() {
        audience.resetTitle();
    }

    @Override
    public void showBossBar(@NotNull BossBar bar) {
        audience.showBossBar(bar);
    }

    @Override
    public void hideBossBar(@NotNull BossBar bar) {
        audience.hideBossBar(bar);
    }

    @Override
    public void playSound(@NotNull Sound sound) {
        audience.playSound(sound);
    }

    @Override
    public void playSound(@NotNull Sound sound, double x, double y, double z) {
        audience.playSound(sound, x, y, z);
    }

    @Override
    public void stopSound(@NotNull Sound sound) {
        audience.stopSound(sound);
    }

    @Override
    public void playSound(@NotNull Sound sound, Sound.@NotNull Emitter emitter) {
        audience.playSound(sound, emitter);
    }

    @Override
    public void stopSound(@NotNull SoundStop stop) {
        audience.stopSound(stop);
    }

    @Override
    public void openBook(Book.@NotNull Builder book) {
        audience.openBook(book);
    }

    @Override
    public void openBook(@NotNull Book book) {
        audience.openBook(book);
    }
}
